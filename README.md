# Project Title: A-HI

## Overview
In this project we have applied some techniques of ML to predict the PM10 index in the city of Bologna.

## Installation and Setup
Instructions on setting up the project environment:
1. Clone the repository: `git clone [repository link]`
2. Install dependencies: `pip install -r requirements.txt`
It's recommended the use of virtual-env.

## Structure
- `/data`: Contains raw and processed data.
- `/src`: Source code for the project.
  - `/scripts`: Individual scripts or modules.
  - `/notebooks`: Jupyter notebooks or similar.
- `/tests`: Test cases for your application.
- `/docs`: Additional documentation in text format (e.g., LaTeX or Word).
- `/public`: Folder where GitLab pages will write static website. 
- `index.html`: Documentation in rich format (e.g., HTML, Markdown, JavaScript), will populate `public`.

## Contact
federico.borci@studio.unibo.it
stefano.staffolani@studio.unibo.it
